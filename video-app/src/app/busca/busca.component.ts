import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppService } from './../app.service';

@Component({
  selector: 'app-busca',
  templateUrl: './busca.component.html',
  styleUrls: ['./busca.component.scss']
})
export class BuscaComponent implements OnInit {

  termo = '';
  busca;
  @Output() resultado = new EventEmitter();

  constructor(private servico: AppService) { }

  ngOnInit() {
  }

  buscar(palavra) {
    this.termo = palavra.target.value;
  }

  inputBuscar() {
    this.resultado.emit(this.termo);
  }
}
