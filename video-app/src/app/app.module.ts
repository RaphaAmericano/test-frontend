import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppService } from './app.service';
import { AppComponent } from './app.component';
import { BuscaComponent } from './busca/busca.component';
import { ItemListaComponent } from './item-lista/item-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    BuscaComponent,
    ItemListaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
