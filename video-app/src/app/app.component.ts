import { Component, ViewChild } from '@angular/core';
import { AppService } from './app.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  lista_videos;
  termo_busca;
  constructor( private servico: AppService) {}

  receberTermo(termo: string ) {
    this.termo_busca = termo;
    this.servico.apiYoutube(this.termo_busca).subscribe(
      res => this.lista_videos = res,
      error => console.log(error)
    )
  }

}
