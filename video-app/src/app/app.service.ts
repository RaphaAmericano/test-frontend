import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AppService {
  private api_key = 'AIzaSyDBAn076k91t-J_0LrDFEom1xb39WKYbQQ';
  constructor( private http: HttpClient ) { }

  apiYoutube(palavra) {
    const url = 'https://www.googleapis.com/youtube/v3/search?part=id,snippet&q='+palavra+'&key='+this.api_key;
    return this.http.get(url);
  }
}
